try:
    from azure.cosmosdb.table.tableservice import TableService
    from azure.cosmosdb.table.models import Entity
    from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
    from matplotlib.figure import Figure
    from tkcalendar import Calendar, DateEntry
    from tkinter import ttk

    import numpy as np
    import pandas as pd
    import tkinter as tk
    import matplotlib.pyplot as plt

    import os
    import yaml
except:
    print("Error in import packages")

try:
    f = open("apikey.yaml", "r")
    f = yaml.load(f, Loader=yaml.FullLoader)
    table_service = TableService(account_name=f['apiname'], account_key=f['apikey'])
except:
    print("Problems accessing keys or account")

vessel_options = ['Oroe', 'Sejeroe', 'Maersk Sandra']
channel_options = ['RPM1', 'RPM2', 'Torque1', 'Torque2', 'Fuelrate1', 'Fuelrate2']

class CreateQueury:
    def __init__(self, vessel, tags, start_time, end_time):
        self.vessel = vessel
        self.tags = tags
        self.start_time = start_time
        self.end_time = end_time

    def CreateDF(self):
        df_holder = {}

    def LoadData(self):
        for tag in self.tags:
            loaded_data[tag] = table_service.query_entities('measurements', filter=f"PartitionKey eq '{self.vessel}' and RowKey ge '{self.start_time}' and RowKey lt '{self.end_time}' and Tag eq '{tag}'")
        self.loaded_data = loaded_data

    def CreateDF(self):
        df_holder = {}
        

if __name__ == '__main__':
    data = CreateQueury('Oroe', ['RPM1', 'RPM2'], '2020-10-4T13:00:0', '2020-10-4T13:30:0')
    data = data.LoadData()