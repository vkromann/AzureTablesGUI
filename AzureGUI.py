from azure.cosmosdb.table.tableservice import TableService
from azure.cosmosdb.table.models import Entity
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
from tkcalendar import Calendar, DateEntry
from tkinter import ttk

import numpy as np
import pandas as pd
import tkinter as tk
import matplotlib.pyplot as plt

import os
import yaml

plt.close()

f = open("apikey.yaml", "r")
f = yaml.load(f, Loader=yaml.FullLoader)
table_service = TableService(account_name="stgecoprodigi", account_key=f['apikey'])


class DataCollector:
    def __init__(self, vessel, time_start, time_end, tags):
        self.vessel = vessel
        self.time_start = time_start
        self.time_end = time_end
        self.tags = tags
    
    def LoadData(self):
        results = {}

        for tag in self.tags:
            results[tag] = table_service.query_entities('measurements', filter=f"PartitionKey eq '{self.vessel}' and RowKey ge '{self.time_start}' and RowKey lt '{self.time_end}' and Tag eq '{tag}'")
        return results

    def CreateDF(self, data):
        df_holder = {}
        self.data = data

        for tasks in self.data:
            _from = []
            _to = []
            _sum = []
            _count = []
            _mean = []

            for task in self.data[tasks]:
                _from.append(task.From)
                _to.append(task.To)
                _sum.append(task.Sum)
                _count.append(task.Count)
                _mean.append(task.Sum/task.Count)

            df_holder[tasks] = pd.DataFrame({'From': _from, 'To': _to, 'Sum': _sum, 'Count': _count, 'Mean' : _mean})
        
        return df_holder


vessel_options = ['Oroe', 'Sejeroe', 'Maersk Sandra']
channel_options = ['RPM1', 'RPM2', 'Torque1', 'Torque2', 'Fuelrate1', 'Fuelrate2']


def fetch(entries):
    channel_list = []
    vessel = entries[0].get()
    channel_list.append(entries[1].get())
    channel_list.append(entries[2].get())
    time_start = f"{cal1}T{time[0].get()}:{time[1].get()}:0"
    time_end = f"{cal2}T{time[2].get()}:{time[3].get()}:0"
    data = DataCollector(vessel, time_start, time_end, channel_list)
    t = data.CreateDF(data.LoadData())
    plot(t)


def create_df(t):
    df = pd.DataFrame(None)
    for x in t:
        df[f'{x}']  = t[x]['Mean']
    return df

def calendar1():
    def print_sel():
        global cal1
        cal1 = cal.selection_get()
        start_date.set(cal1)
        top.destroy()
        

    top = tk.Toplevel(root)

    cal = Calendar(top,
                   font="Arial 14", selectmode='day',
                   cursor="hand1")

    cal.pack(fill="both", expand=True)
    ttk.Button(top, text="ok", command=print_sel).pack()

def calendar2():
    global cal2
    def print_sel():
        global cal2
        cal2 = cal.selection_get()
        end_date.set(cal2)
        top.destroy()
        

    top = tk.Toplevel(root)

    cal = Calendar(top,
                   font="Arial 14", selectmode='day',
                   cursor="hand1")

    cal.pack(fill="both", expand=True)
    ttk.Button(top, text="ok", command=print_sel).pack()


def plot(t):
    fig = Figure(figsize=(10, 8), dpi=100)
    top = tk.Toplevel(root)
    fig.add_subplot(111).plot(create_df(t))
    canvas = FigureCanvasTkAgg(fig, master=top)
    canvas.draw()
    canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=1)

    toolbar = NavigationToolbar2Tk(canvas, top) 
    toolbar.update()
    canvas.get_tk_widget().pack()

def makeform(root):
    entries = []
    row = tk.Frame(root)
    variable = tk.StringVar(row)
    variable.set(vessel_options[0])
    channels1 = tk.StringVar(row)
    channels1.set(channel_options[0])
    channels2 = tk.StringVar(row)
    channels2.set(channel_options[0])

    lab_vessel = tk.Label(row, width=15, text="Vessel", anchor='w')
    lab_channel1 = tk.Label(row, width=15, text="First Channel", anchor='w')
    lab_channel2 = tk.Label(row, width=15, text="Second Channel", anchor='w')

    v = tk.OptionMenu(row, variable, *vessel_options)
    c1 = tk.OptionMenu(row, channels1, *channel_options)
    c2 = tk.OptionMenu(row, channels2, *channel_options)
    
    v.configure(bg = "#059fad", activebackground = "#04b4c4", width=18)
    c1.configure(bg = "#059fad", activebackground = "#04b4c4", width=18)
    c2.configure(bg = "#059fad", activebackground = "#04b4c4", width=18)

    row.grid(row=0, column=0, sticky=tk.W)
    

    lab_vessel.grid(sticky=tk.W)
    lab_channel1.grid(sticky=tk.W)
    lab_channel2.grid(sticky=tk.W)

    v.grid(row=0, column=1)
    c1.grid(row=1,  column=1)
    c2.grid(row=2,  column=1)


    entries.append(variable)
    entries.append(channels1)
    entries.append(channels2)

    return entries

if __name__ == '__main__':
    global time
    time = []
    root = tk.Tk()
    root.wm_title("DataLogger Viz")

    s = ttk.Style(root)
    s.theme_use('clam')

    ents = makeform(root)
    root.bind('<Return>', (lambda event, e=ents: fetch(e)))   

    buttonframe = tk.Frame(root)
    buttonframe.grid(row=1, column=0, sticky=tk.W)

    lab_start = tk.Label(buttonframe, width=15, text="Start Time", anchor='w')
    lab_end = tk.Label(buttonframe, width=15, text="End Time", anchor='w')
    lab_date = tk.Label(buttonframe, width=15, text="Choose date", anchor='w')
    lab_hour = tk.Label(buttonframe, width=15, text="Hour", anchor='w')
    lab_min = tk.Label(buttonframe, width=15, text="Minute", anchor='w')

    lab_start.grid(row=1, column=0, sticky=tk.W)
    lab_end.grid(row=2, column=0, sticky=tk.W)
    lab_date.grid(row=0, column=1, sticky=tk.W)
    lab_hour.grid(row=0, column=2, sticky=tk.W)
    lab_min.grid(row=0, column=3, sticky=tk.W)
    
    en_hour1 = tk.Entry(buttonframe)
    time.append(en_hour1)
    en_min1 = tk.Entry(buttonframe)
    time.append(en_min1)

    en_hour2 = tk.Entry(buttonframe)
    time.append(en_hour2)
    en_min2 = tk.Entry(buttonframe)
    time.append(en_min2)

    en_hour1.grid(row=1, column=2)
    en_min1.grid(row=1, column=3)

    en_hour2.grid(row=2, column=2)
    en_min2.grid(row=2, column=3)

    start_date = tk.StringVar(buttonframe)
    start_date.set("Calendar")

    end_date = tk.StringVar(buttonframe)
    end_date.set("Calendar")

    b1 = ttk.Button(buttonframe, text='Show', command=(lambda e=ents: fetch(e)))
    b1.grid(row=3, column=0, sticky=tk.W)
    b2 = ttk.Button(buttonframe, text='Quit', command=root.quit)
    b2.grid(row=3, column=1, sticky=tk.W)
    ttk.Button(buttonframe, textvariable=start_date, command=calendar1).grid(row=1, column=1, sticky=tk.W)
    ttk.Button(buttonframe, textvariable=end_date, command=calendar2).grid(row=2, column=1, sticky=tk.W)

    root.mainloop()

# Run window
tk.mainloop()